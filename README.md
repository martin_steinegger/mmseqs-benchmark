# MMseqs2 benchmark

MMseqs2-benchmark contains scripts and programs used to benchmark to produce the data in the mmseqs2 paper.

# Requirements

- CMAKE is required to install 
- gcc/g++ version 4.8 or higher
- 128 GB RAM and AVX2

# Setup & Run 
We provide all fasta databases, evaluation tools, tested method binaries (excluding UBLAST) and scripts as tar file (mmseqs2-benchmark.tar.gz). 

    :::bash
    wget http://wwwuser.gwdg.de/~compbiol/mmseqs2/mmseqs2-benchmark.tar.gz
	tar xvfz mmseqs2-benchmark.tar.gz
	mkdir -p mmseqs2-benchmark/mmseqs-benchmark/build && cd mmseqs2-benchmark/mmseqs-benchmark/build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make 
    cd ../.. 
	
To run the sensitivity benchmark call the respective script for each tool

    :::bash
    scripts/run_<toolname>.sh
   
After the run is done the BLAST tab results are in the results/<toolname> and the plotting data is in  plots/<toolname> folder.

The speed benchmark can be run with the scripts with the _speed.sh suffix.

    :::bash
    scripts/run_<toolname>_speed.sh
   

# Tools

This repositor also contains all tools tools to reannoated uniref sequences. 

create_dataset <annotation.m8> <sequences> <output>
evaluate_results <annotatede_query.fas> <annotatede_db.fas> <result.m8> <output_plots> <result list lenght> <defines fraction up of TP to the X FP>