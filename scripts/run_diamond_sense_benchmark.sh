#!/bin/sh -ex
#BSUB -q mpi
#BSUB -o out.%J
#BSUB -e err.%J
#BSUB -W 48:00
#BSUB -m hh
#BSUB -a openmp 
#BSUB -n 16 
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -R "span[ptile=16]"
export PATH=$PATH:/cbscratch/martin/mmseqs2.0_bench/tools/:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas
#diamond makedb --in $BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas -d $BENCHDIR/db/diamond/db
mkdir -p /local/diamond/
# copy to ssd to be realistic
time cp $BENCHDIR/db/diamond/db* /local/diamond/
time diamond blastp -q $QUERY -d /local/diamond/db -a $BENCHDIR/results/diamond/result_diamond_sens -t /dev/shm --max-target-seqs 4000 --evalue 10000.0 --threads 16 -v --sensitive
time diamond view -a $BENCHDIR/results/diamond/result_diamond_sens.daa -o $BENCHDIR/results/diamond/result_diamond_sens.m8
evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/diamond/result_diamond_sens.m8  $BENCHDIR/plots/diamond_sens_roc5_30mio.dat 4000 1
