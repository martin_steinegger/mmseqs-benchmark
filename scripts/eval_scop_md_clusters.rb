#!/usr/bin/ruby
require 'optparse'


class Eval

    def run(args)
        @kclustSuf = ".dmp"
        @uclustSuf = ".uc"
        @cdhitSuf = ".clstr"
        @mmSeqsSuf = ""
        @blastclustSuf = ".blast"
        @tsvSuf = ".tsv"
        @clu_template_file = ""
#        @delete_old = false


	parse
        @clu_file_list = @clus_query.split(",");

        # 2 domains of the protein -> seqnum
        @doms2id = Hash.new
        # seqnum -> 2 domains of the protein
        @id2doms = Array.new
        # seqnum -> header
        @id2header = Array.new
        # kClust clustering representation: seqnum -> representative of the cluster seqnum
        @id2rep_template = Array.new()
        # array of arrays with clusters
        @clus_query = Array.new(@clu_file_list.size) { |a| a = Array.new}
        # array with kClust clustering representation for each clustering method
        @id2rep_query = Array.new(@clu_file_list.size) { |a| a = Array.new}
        # array with template clusters
        @clus_template = Array.new
        # number of clusters for each clustering
        @clu_size_list = Array.new(@clu_file_list.size+1)
        # clan assignment for PFAM families (only PFAM!)
        @id2clan = Array.new

        @clu_template_file = File.expand_path(@clu_template_file)
        puts "Init " + @clu_template_file
        initFastaDatabase(@clu_template_file, @id2rep_template)
        seqnum = @id2rep_template.size

        @clu_file_list.each_with_index { |clu_f, idx|
            puts "Reading " + clu_f
            clu_f = File.expand_path(clu_f)
            if File.extname(clu_f) == @kclustSuf
                readKclust(clu_f, @clus_query[idx])
                @clu_size_list[idx+1] = @clus_query[idx].size
            elsif File.extname(clu_f) == @uclustSuf
                readUclust(clu_f, @clus_query[idx])
                @clu_size_list[idx+1] = @clus_query[idx].size
            elsif File.extname(clu_f) == @cdhitSuf
                readCdhit(clu_f, @clus_query[idx], seqnum)
                @clu_size_list[idx+1] = @clus_query[idx].size
            elsif File.extname(clu_f) == @mmSeqsSuf
                readMMSeqs(clu_f, @clus_query[idx])
                @clu_size_list[idx+1] = @clus_query[idx].size
            elsif File.extname(clu_f) == @blastclustSuf
                readBlastclust(clu_f, @clus_query[idx])
                @clu_size_list[idx+1] = @clus_query[idx].size
            elsif File.extname(clu_f) == @tsvSuf
                readTsvclust(clu_f, @clus_query[idx])
                @clu_size_list[idx+1] = @clus_query[idx].size
            else
                puts "Unknown file format for file " + clu_f + "\nIgnoring the file."
            end
        }

        puts
        puts "-----Evaluating FP-------\n\n"
        evalFPpairs
=begin
        puts "Transforming data structures"
        if @delete_old
            @id2rep_template.clear
        end
        @clus_query.each_with_index { |cl, idx|
            transformClusteringRepresentation(cl, @id2rep_query[idx], seqnum, @delete_old)
        }
        @clu_size_list[0] = @clus_template.size

        puts
        puts "-----Evaluating FN------\n"
        evalFNpairs
=end
        puts
        puts "------------------------\n"

        puts "# clusters in " + @clu_template_file + ":"
        puts @clu_size_list[0]
        @clu_file_list.each_with_index { |clu_f, idx|
            puts "# clusters in " + clu_f + ":"
            puts @clu_size_list[idx+1]
        }
    end

    def evalFPpairs()
        @clus_query.each_with_index { |clu, idx|
            
            fp = 0
            corrupted_num = 0
            wrong_seqs_num = 0
            clu.each{ |c|
                corrupted = 0
                reps = Array.new
                clu_string = ""
                for i in 0...c.size
                    reps << @id2rep_template[c[i]]
                    clu_string << @id2header[c[i]] << "\n"
                    for j in i+1...c.size
                        same_clan = (@id2clan[c[i]] == @id2clan[c[j]] && @id2clan[c[i]] != nil && @id2clan[c[j]] != nil)
                        if @id2rep_template[c[i]] != @id2rep_template[c[j]] && !same_clan
#                            puts "__________________________________" if corrupted == 0
#                            puts @id2doms[c[i]] + " " +@id2doms[c[j]] 
                            fp = fp+1
                            corrupted = 1
                        end
                    end
                end
                if corrupted == 1
                    puts "Cluster:"
                    puts clu_string
                    reps_uniq = reps.uniq
                    largest_clu = 0
                    for i in 0...reps_uniq.size
                        largest_clu = [reps.count(reps_uniq[i]), largest_clu].max
                    end
                    puts "# wrong sequences: " + (reps.size - largest_clu).to_s
                    puts
                    wrong_seqs_num = wrong_seqs_num + reps.size - largest_clu
                end
                corrupted_num = corrupted_num + corrupted
            }
            puts @clu_file_list[idx]
            puts "FP pairs: " + fp.to_s
            puts "#corrupted clusters: " + corrupted_num.to_s
            puts "#wrong sequences: " + wrong_seqs_num.to_s
            puts "#wrong sequences per corrupted cluster: " + (wrong_seqs_num.to_f/corrupted_num.to_f).to_s
            puts
            puts "_________________________________________________________________________\n"
            puts
        }
    end

    # clu_file_list -> kClust clustering representation array
    def transformClusteringRepresentation(clus, ar, seqnum, delete_source=false)
        clus.each { |c|
           # p c
            rep = c.min
            c.each { |m|
                ar[m] = rep
            }
        }
        if delete_source
            clus.clear
        end
    end

    def evalFNpairs()
        fn_pairs = Array.new(@clu_file_list.size) { |p| p = 0 }
        @clus_template.each { |c|
            for i in 0...c.size
                for j in i+1...c.size
                    for f in 0...@clu_file_list.size
                        if @id2rep_query[f][c[i]] != @id2rep_query[f][c[j]]
                            fn_pairs[f] = fn_pairs[f]+1
                        end
                    end
                end
            end
        }
        fn_pairs.each_with_index { |p, i|
            puts
            puts @clu_file_list[i]
            puts "FN pairs: " + p.to_s
        }
    end

    def initFastaDatabase(filename, ar)
        superfams2rep = Hash.new
        superfams2clu = Hash.new
        db_f = File.new(filename, 'r')
        id = 0
        cluid = 0
        while line = db_f.gets
            if line[0,1] == ">"
                split_ar = line.split(' ')
                
                doms = split_ar[0]
                doms = doms[1,doms.length]
             
                fams = split_ar[1]
                clan = ""
                superfams = ""
                # only possible for PFAM benchmark
                if fams.index("|")
                    split_ar1 = fams.split("|")
                    fams = split_ar1[0]
                    clan = split_ar1[1]
                    @id2clan[id] = clan
                    # PFAM has no superfamilies
                    superfams = fams
                # SCOP dataset, parse superfams
                else
                    superfams = fams.split(",").collect { |fam| fam.sub(/\.\d+$/, '') } .join(",")
                end

                if !superfams2rep[superfams]
                    superfams2rep[superfams] = id
                    ar[id] = id
                    
                    superfams2clu[superfams] = cluid
                    clu = Array.new
                    clu << id
                    @clus_template << clu
                    cluid = cluid + 1
                else
                    ar[id] = superfams2rep[superfams]
                    @clus_template[superfams2clu[superfams]] << id
                end

                @doms2id[doms] = id
                @id2doms[id] = doms
                @id2header[id] = line.chomp[1,line.length]
                id = id + 1
            end
        end
        @clu_size_list[0] = @clus_template.size
    end

    def readKclust(filename, ar)
        clu_f = File.new(filename, 'r')
        db_f = File.new(File.dirname(filename) + "/" + "db_sorted.fas", 'r');
        if !File.exists?(db_f.path)
            puts "Directory of kClust clustering file " + @clutemplate + " used as template does not contain db_sorted.fas file. Exiting.";
            exit
        end

        loc_id2doms = Array.new
        id = 0
        while line = db_f.gets
            if line[0,1] == ">" 
                doms = line.split(' ')[0]
                doms = doms[1,doms.length]
                loc_id2doms[id] = doms
                id = id + 1
            end
        end

        prev_rep_id = 0
        curr_clu = Array.new
        while line = clu_f.gets
            next if line[0,1] == "#"
            line.match(/(\d+) (\d+)/)
            rep_id = $2.to_i-1
            if rep_id != prev_rep_id
                ar << curr_clu;
                curr_clu = Array.new
                prev_rep_id = rep_id
            end

            curr_clu << @doms2id[loc_id2doms[$1.to_i-1]]
        end
        ar << curr_clu;
#        puts "kClust:"
#        p ar
#        puts
    end

    def readMMSeqs(filename, ar)
        clu_f = File.new(filename, 'r')
        while (line = clu_f.readline("\0")) && !clu_f.eof?
            curr_clu = Array.new
            clu_members = line.split("\n")[0...-1]
            clu_members.each { |m|
                curr_clu << @doms2id[m]
            }
            ar << curr_clu
        end
    end

    def readTsvclust(filename, ar)
        clu_f = File.new(filename, 'r')
        currId = ""
        curr_clu = Array.new
        while line = clu_f.gets
            cutId = line.split(/\s+/)[0] + "\n"
            if !(cutId.eql? currId)
                currId = cutId
                if curr_clu.size == 0
                   next
                end  
                ar << curr_clu
                curr_clu = Array.new
                doms = line.split(/\s+/)[1]
                curr_clu.push(@doms2id[doms])
            else
                doms = line.split(/\s+/)[1]
                curr_clu.push(@doms2id[doms])
               # seq_control[@doms2id[doms]] = 1
            end
	end
	ar << curr_clu
    end

    def readBlastclust(filename, ar)
        clu_f = File.new(filename, 'r')
        while line = clu_f.gets
            curr_clu = Array.new
            clu_members = line.split(" ")
            clu_members.each { |m|
            curr_clu << @doms2id[m]
            }
            ar << curr_clu
        end
    end

    def readUclust(filename, ar)
        clu_f = File.new(filename, 'r')
        while line = clu_f.gets
=begin            # this was for parsing old uclust output!
            if line =~ /([SH])\t(\d+)\t\d+\t[\d\.\*]{1,4}\t[\.\*]\t[\*\d]+\t[\*\d]+\t+[\*\dA-Z]+\t(\S+) .*/
                if $1 == "S"
                    ar << Array.new
                end
            ar[$2.to_i] << @doms2id[$3]
            end
=end
                linear = line.split("\t")
                if linear[0] == "C"
                    next
                elsif linear[0] == "S"
                    ar << Array.new
                end
                ar[linear[1].to_i] << @doms2id[linear[8]]
        end
    end

    def readCdhit(filename, ar, seqnum)
        clu_f = File.new(filename, 'r')
        # control for missing sequences
        seq_control = Array.new(seqnum) { |a| a=0 }
        curr_clu = Array.new
        while line = clu_f.gets
            if line[0,1] == ">"
                next if curr_clu.size == 0
                ar << curr_clu
                curr_clu = Array.new
            elsif line[0,3] == "..."
                next
            else
                doms = line.split(/\s+/)[2]
                doms = doms[1,doms.length]
                curr_clu.push(@doms2id[doms])
                seq_control[@doms2id[doms]] = 1
            end
        end
        ar << curr_clu
        seq_control.each_with_index { |e, i|
            if e == 0
                ar << [i]
                puts "Swissprot sequence " << @id2doms[i].to_s << " missing in CD-HIT clustering! Considering it a singleton."
            end
        } 
#        puts "Cdhit:"
#        p ar
#        puts
    end
    
    def parse()
	OptionParser.new do |o|
	    o.banner = "Usage example: eval_scop_md_clusters.rb -t clusters.dmp -c clusters.uc,clusters.clstr"
	    o.define_head( "Evaluate clusters from kClust, Uclust and cdhit runs on the SCOP 2-domain benchmark or Pfam clustering benchmark.\n")

	    o.on("-d", "--db-template FILE", String, "Initial DB file.") { |val| @clu_template_file = val }
            o.on("-c", "--clustering-files FILELIST", String, "Clustering files separated with commas.") { |val| @clus_query = val }
	    o.on("-h", "--help", "Show this help message.") { puts o; exit }
	end.parse!
    end

end

Eval.new.run(ARGV)
