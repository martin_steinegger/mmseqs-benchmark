#!/bin/sh -ex
#BSUB -q mpi
#BSUB -o out.%J
#BSUB -e err.%J
#BSUB -W 48:00
#BSUB -m hh
#BSUB -a openmp
#BSUB -n 16 
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -env "all"
#BSUB -R "span[ptile=16]"
export PATH=$PATH:/cbscratch/martin/mmseqs2.0_bench/tools:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas
#rm -f $BENCHDIR/db/last/*
#time lastdb -p -cR01 $BENCHDIR/db/last/db $DB -p -v 
#mkdir /local/last/
#time cp $BENCHDIR/db/last/db30Mio* /local/last/
time /cbscratch/martin/mmseqs2.0_bench/tools/last-712/src/lastal /local/last/db30Mio $QUERY -u3 -D100  -P 16 -f BlastTab > $BENCHDIR/results/last/results.m8 
grep -v "#" $BENCHDIR/results/last/results.m8 > $BENCHDIR/results/last/results_clean.m8
evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/last/results_clean.m8 $BENCHDIR/plots/last_30mio_roc5.dat 4000 1
