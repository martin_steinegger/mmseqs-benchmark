#!/bin/sh -ex
#BSUB -q mpi
#BSUB -o out.%J
#BSUB -e err.%J
#BSUB -W 48:00
#BSUB -m hh
#BSUB -a openmp 
#BSUB -n 16 
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -R "span[ptile=16]"
export PATH=/cbscratch/martin/mmseqs2.0_bench/tools/ncbi-blast-2.2.31+-src/c++/ReleaseMT/bin:$PATH:/cbscratch/martin/mmseqs2.0_bench/tools:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas
#time makeblastdb -in $DB -parse_seqids -dbtype prot -out $BENCHDIR/db/blast/db30Mio.fas
mkdir /local/sword
# copy to ssd to be realistic
time cp $DB /local/sword/db.fas
# -e double  : threshold of log10(E-value)/E-value [default: 1.0/10.0]. It is the default threshold.
# log10(10000)/10000 = 0.0004
# -i ../../db/query.fas -j /local/sword/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas -o result_sword_local.m8 -t 16 -a 4000 --evalue 10000
time $BENCHDIR/tools/sword/sword -i $QUERY -j /local/sword/db.fas -o $BENCHDIR/results/sword/results_sword_sense.m9  -t 16 -a 4000 --evalue 10000 
grep -v "^Query" $BENCHDIR/results/sword/results_sword_sense.m9 | grep -v "#" > $BENCHDIR/results/sword/results_sword_sense.m8
evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/sword/results_sword_sense.m8  $BENCHDIR/plots/sword_sense_roc5_30mio.dat 4000 1
