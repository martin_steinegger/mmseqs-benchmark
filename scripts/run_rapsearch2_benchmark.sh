#!/bin/sh -ex
#BSUB -q mpi
#BSUB -o out.%J
#BSUB -e err.%J
#BSUB -W 48:00
#BSUB -m hh
#BSUB -a openmp 
#BSUB -n 16 
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -R "span[ptile=16]"
export PATH=/cbscratch/martin/mmseqs2.0_bench/tools/ncbi-blast-2.2.31+-src/c++/ReleaseMT/bin:$PATH:/cbscratch/martin/mmseqs2.0_bench/tools:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas
#time makeblastdb -in $DB -parse_seqids -dbtype prot -out $BENCHDIR/db/blast/db30Mio.fas
mkdir /local/rapsearch2
# copy to ssd to be realistic
time cp $BENCHDIR/db/rapsearch2/db30Mio.fas* /local/rapsearch2
# -e double  : threshold of log10(E-value)/E-value [default: 1.0/10.0]. It is the default threshold.
# log10(10000)/10000 = 0.0004
time $BENCHDIR/results/rapsearch2/RAPSearch2.23_64bits/bin/rapsearch -q $QUERY -d /local/rapsearch2/db30Mio.fas -o $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio -v 4000  -z 16 -e 4 -t a -b 0 
grep -v "^#" $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio.m8 > $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio_clean.m8
awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"10^$11"\t"$12 }' $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio_clean.m8 > $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio.m8
evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio.m8  $BENCHDIR/plots/rapsearch2_roc5_30mio.dat 4000 1
