#!/bin/sh -ex
#BSUB -q mpi-long+
#BSUB -o out.%J
#BSUB -e err.%J
#BSUB -W 48:00
#BSUB -m hh
#BSUB -a openmp
#BSUB -n 16 
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -env "all"
#BSUB -R "span[ptile=16]"
export PATH=/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-dev/build/src/:$PATH:/cbscratch/martin/mmseqs2.0_bench/tools:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
export MMDIR=/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-dev/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas
RUN=$RANDOM
#rm -f $BENCHDIR/db/mmseqs_test/db* 
#rm -f $BENCHDIR/db/mmseqs_test/query* 
#rm -f $BENCHDIR/results/mmseqs_test/results_pref* 
#rm -f $BENCHDIR/results/mmseqs_test/results_aln*
#time mmseqs createdb $DB $BENCHDIR/db/mmseqs_test/db
#time mmseqs createdb $QUERY $BENCHDIR/db/mmseqs_test/query
#time mmseqs createindex $BENCHDIR/db/mmseqs_test/db $BENCHDIR/db/mmseqs_test/dbIdxK7 -k 7 --search-mode 1
mkdir $BENCHDIR/results/mmseqs_test/$RUN
time mmseqs search $BENCHDIR/db/mmseqs_test/query $BENCHDIR/db/mmseqs_test/lowcomplexdb/db $BENCHDIR/results/mmseqs_test/results_aln_$RUN $BENCHDIR/results/mmseqs_test/$RUN -e 10000.0 --e-profile 0.001 -k 7 --k-score 95 --max-seqs 4000 --use-index
time mmseqs formatalignment $BENCHDIR/db/mmseqs_test/query $BENCHDIR/db/mmseqs_test/db $BENCHDIR/results/mmseqs_test/results_aln_$RUN $BENCHDIR/results/mmseqs_test/results_aln_$RUN".m8"
sort -k1,1 -k11,11g $BENCHDIR/results/mmseqs_test/results_aln_$RUN".m8" > $BENCHDIR/results/mmseqs_test/results_aln_sorted_$RUN".m8"
mv $BENCHDIR/results/mmseqs_test/results_aln_sorted_$RUN".m8" $BENCHDIR/results/mmseqs_test/results_aln_$RUN".m8"
evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/mmseqs_test/results_aln_$RUN".m8" $BENCHDIR/plots/mmseqs_test_profile_${RUN}_roc5.dat 4000 1
