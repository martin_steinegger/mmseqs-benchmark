#!/bin/bash -ex
#BSUB -J blast[1-2000]
#BSUB -q mpi-short
#BSUB -o out.%I.%J
#BSUB -e err.%I.%J
#BSUB -W 2:00
#BSUB -m hh
#BSUB -n 1
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -R "span[ptile=16]"
export PATH=$PATH:/cbscratch/martin/mmseqs2.0_bench/tools:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas
#time makeblastdb -in $DB -parse_seqids -dbtype prot -out $BENCHDIR/db/blast/db30Mio.fas
#mkdir /local/ssearch/
# copy to ssd to be realistic
#time cp $BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas /local/ssearch/db30Mio.fas
# -e double  : threshold of log10(E-value)/E-value [default: 1.0/10.0]. It is the default threshold.
# log10(10000)/10000 = 0.0004
/cbscratch/martin/mmseqs2.0_bench/tools/ffindex/build/src/ffindex_get -n /cbscratch/martin/mmseqs2.0_bench/results/ssearch/query /cbscratch/martin/mmseqs2.0_bench/results/ssearch/query.index ${LSB_JOBINDEX} | \
$BENCHDIR/tools/ssearch/bin/ssearch36 -f BP62 -f -11 -g -1 -m 8 -E 10000 -T 1 -z 3 @ $BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas > $BENCHDIR/results/ssearch/${LSB_JOBINDEX}.ssearch2 
#cat $BENCHDIR/results/ssearch/*.ssearch2 > $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio_single.m8
#evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/rapsearch2/results_rapsearch2_30mio_single.m8  $BENCHDIR/plots/rapsearch2_roc5_30mio_single.dat 4000 1
