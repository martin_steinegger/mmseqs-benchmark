#!/bin/sh -ex
#BSUB -q mpi
#BSUB -o out.%J
#BSUB -e err.%J
#BSUB -W 48:00
#BSUB -m hh
#BSUB -a openmp 
#BSUB -n 16 
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -R "span[ptile=16]"
export PATH=$PATH:/cbscratch/martin/mmseqs2.0_bench/tools:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/dbSwipeHHblitsUniref50ShuffelLinkerPlus27Inverse.fas
#$BENCHDIR/tools/usearch7.0.1090_i86linux64 -makeudb_ublast $DB --output $BENCHDIR/db/usearch/db27Mio.udb
mkdir /local/usearch/
# copy to ssd to be realistic
time cp $BENCHDIR/db/usearch/db.udb /local/usearch/db.udb
time $BENCHDIR/tools/usearch7.0.1090_i86linux64 -ublast $QUERY -db /local/usearch/db.udb -blast6out $BENCHDIR/results/ublast/results_ublast_30mio_raw.m8 -threads 16 -evalue 10000.0 
awk '{printf("%s", $1); for(i = NF - 10; i < NF; i++){printf("\t%s",$i)} printf("\n") }' $BENCHDIR/results/ublast/results_ublast_30mio_raw.m8 > $BENCHDIR/results/ublast/results_ublast_30mio.m8
evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/ublast/results_ublast_30mio.m8  $BENCHDIR/plots/ublast_roc5_30mio.dat 4000 1
