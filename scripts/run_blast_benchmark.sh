#!/bin/sh -ex
#BSUB -q mpi-long
#BSUB -o out.%J
#BSUB -e err.%J
#BSUB -W 120:00
#BSUB -m hh
#BSUB -a openmp 
#BSUB -n 16 
#BSUB -R haswell
#BSUB -R cbscratch
#BSUB -R "span[ptile=16]"
export PATH=/cbscratch/martin/mmseqs2.0_bench/tools/ncbi-blast-2.2.31+/bin:$PATH:/cbscratch/martin/mmseqs2.0_bench/tools:/cbscratch/martin/mmseqs2.0_bench/tools/mmseqs-benchmark/build/
BENCHDIR=/cbscratch/martin/mmseqs2.0_bench/
QUERY=$BENCHDIR/db/query.fas
DB=$BENCHDIR/db/uniprot20_2012_10.fas
time makeblastdb -in $DB -parse_seqids -dbtype prot -out $BENCHDIR/db/blast/db27Mio.fas
mkdir /local/blast
# copy to ssd to be realistic
time cp $BENCHDIR/db/blast/db27Mio.fas* /local/blast
time psiblast -db /local/blast/db27Mio.fas -query $QUERY -num_descriptions 4000 -num_alignments 4000 -num_threads 16 -evalue 10000.0 -out $BENCHDIR/results/blast/results_blast_30mio.m8 -outfmt 6 
evaluate_results $QUERY $BENCHDIR/db/db.fas $BENCHDIR/results/rapsearch2/results_blast_30mio.m8  $BENCHDIR/plots/blast_roc5_30mio.dat 4000 1
