from itertools import groupby
import random
from optparse import OptionParser
from math import floor
def reverse_string(a_string):
    return a_string[::-1]
def shuffel_string(a_string):
    return ''.join(random.sample(a_string,len(a_string)))

def shuffel_string_window(a_string):
    s = list(a_string)
    strLen = len(a_string)
    posArray = [0] * strLen
    for i in range(strLen):
        posArray[i] = i
    for i in range(strLen):
        start = max(0, i - 10)
        end = min(strLen - 1, i + 10)
        exchangePos = random.randint(start,end)
        while (posArray[exchangePos] == i) and strLen != 1: 
            exchangePos = random.randint(start,end)
        tmp = s[i]
        s[i] = s[exchangePos]
        s[exchangePos] = tmp
        tmp = posArray[i]
        posArray[i] = posArray[exchangePos]
        posArray[exchangePos] = tmp
        
    return "".join(s)

def fasta_iter(fasta_name):
    """
    given a fasta file. yield tuples of header, sequence
    """
    fh = open(fasta_name)
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = header.next()[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in faiter.next())
        yield header, seq



parser = OptionParser()

parser.add_option("-i", "--in", dest="fasta",
                  help="Path to fasta file.", metavar="STRING")
(options, args) = parser.parse_args()



for header, seq in fasta_iter(options.fasta):
    #0                        1            2                3           4
    #U6GAD1 a.2.5.1 a.52.1.3 |0.0013 0.47 |d1fxka_ d1psya_ |3-99 5-106 |393-498 399-510
    domains = header.split("|")[0].split(" ")
    evalues = header.split("|")[1].split(" ")
    queries = header.split("|")[2].split(" ")
    queriesPos = header.split("|")[3].split(" ")
    targetPos = header.split("|")[4].split(" ")



    domainsStr = ""
    evaluesStr = ""
    queriesStr = ""
    queriesPosStr = ""
    targetPosStr = ""
    last_end = 0
    newSeq = ""
    for splitIdx in range(len(targetPos)):
        tPos = targetPos[splitIdx].split('-')
        evalue = float(evalues[splitIdx])
        
        domainsStr += domains[splitIdx+1] + " "
        evaluesStr += evalues[splitIdx] + " "
        queriesStr += queries[splitIdx] + " "
        queriesPosStr += queriesPos[splitIdx] + " "
        targetPosStr += targetPos[splitIdx] + " "     
        tPos_start = int(tPos[0]) - 1
        tPos_end = int(tPos[1])
        #if evalue > 0.001: # just keep really good conserved domains
        #    continue
        #print tPos_start, tPos_end,
        # handle overlap
        if  tPos_start < last_end:
            #print "start < last end"
            tPos_start = last_end
        if tPos_end < last_end:
            #print "end < last end"
            tPos_end = last_end
            continue
        #print
        #newSeq += reverse_string(seq[last_end:tPos_start])
        newSeq += shuffel_string_window(seq[last_end:tPos_start])
        
        newSeq += seq[tPos_start:tPos_end]
        last_end = tPos_end
    newSeq += shuffel_string_window(seq[last_end:len(seq)])
    print ">"+domains[0] + " " + domainsStr + "|" + evaluesStr + "|" + queriesStr + "|" + queriesPosStr + "|" + targetPosStr
    print newSeq


