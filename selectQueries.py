import glob
import csv
from operator import itemgetter 
def reverse_string(a_string):
    return a_string[::-1]

def findOverlap(hits):
    for i in range(len(hits)):
        for j in range(i + 1, len(hits)):
            tPos_i = hits[i]['templatepos'].split('-') 
            tPos_i_start = int(tPos_i[0])
            tPos_i_end = int(tPos_i[1])
            tPos_j = hits[j]['templatepos'].split('-')
            tPos_j_start = int(tPos_j[0])
            tPos_j_end = int(tPos_j[1])
            max_start_pos = max(tPos_i_start, tPos_j_start) 
            if max_start_pos < tPos_i_end and max_start_pos < tPos_j_end:
                return 1
    return 0
def getTargetPosStart(hit):
    return hit['templatepos'].split('-')[0]

def sortByTargetStart(hits):
    hitsSorted = []
    for tmpHit in hits:
        hitsSorted.append(tmpHit)
    unordered = 1
    while unordered == 1:
        for i in range(len(hitsSorted)):
            hit_i = hitsSorted[i]
            hit_i_start = int(hit_i['templatepos'].split('-')[0])
            unordered = 0
            for j in range(i + 1, len(hitsSorted)):
                hit_j = hitsSorted[j]
                hit_j_start = int(hit_j['templatepos'].split('-')[0])
                if hit_j_start < hit_i_start:
                    tmp = hitsSorted[i]
                    hitsSorted[i] = hitsSorted[j]
                    hitsSorted[j] = tmp
                    unordered = 1
    return hitsSorted
# read query seq len
queryDictLen = dict()
with open("./db/query.fas", "r") as fasta:
    lines = fasta.readlines()
    for i in range(len(lines)):
        line = lines[i]
        if  line.startswith(">"):
            id = line.split()[0]
            idLen = len(id)
            id = id[1:idLen]
            queryDictLen[id] = len(lines[i+1])

# read db seq
dbSeqDict = dict()
with open("./db/db.fas", "r") as fasta:
    lines = fasta.readlines()
    for i in range(len(lines)):
        line = lines[i]
        if  line.startswith(">"):
            lineSplit = line.split()
            id = lineSplit[0]
            idLen = len(id)
            id = id[1:idLen]
            dbSeqDict[id] = (" ".join(lineSplit[1:len(lineSplit)]), lines[i+1].rstrip())
# select hits
queryHitLookup = dict()
with open("scop25_single_seq_results_swipe_0110_awk_newfam", "r") as cvsfile:
    reader = csv.DictReader(cvsfile, delimiter = ' ')
    reader.fieldnames = [field.strip().lower() for field in reader.fieldnames]
    #Query Scop Hit Prob E-value P-value Score SS Cols QueryPos TemplatePos
    for row in reader:

        if float(row['e-value']) < 1E-5:
            qPos = row['querypos'].split('-')
            tPos = row['templatepos'].split('-')
            qLen = queryDictLen[row['query']]
            qcov = (min(float(qLen), float(qPos[1]) - float(qPos[0])) / float(qLen))
	    #print qcov, qPos, qLen
            if qcov > 0.8:
                hitKey = row['hit'].split("|")[1]
                if hitKey in dbSeqDict:
                    if row['query'] not in queryHitLookup:
                        queryHitLookup[row['query']] = []
                    queryHitLookup[row['query']].append(row)

hitLookup = dict()
for query in queryHitLookup:
    for hit in queryHitLookup[query]:
        #if hit not in checkifexist:
        hitKey = hit['hit'].split('|')[1]
#        print hitKey, hit['scop'], hit['querypos'], hit['templatepos'], hit['e-value']
        if hitKey not in hitLookup:
            hitLookup[hitKey] = []
        hitLookup[hitKey].append(hit)
        #checkifexist[hit] = 1;
        break

for hitId in hitLookup:
    hits = hitLookup[hitId]

    header = dbSeqDict[hitId][0]
    dbSeq = dbSeqDict[hitId][1]
    #newDbSeq = ""
    hitsSorted = sortByTargetStart(hits)

    #print hitId
    scop_fam = ""
    query = ""
    querypos = ""
    targetpos = ""
    evals = ""
    #last_end = 0
    for hit in hitsSorted:
    #    tPos = hit['templatepos'].split('-') 
    #    tPos_start = int(tPos[0]) - 1
    #    tPos_end = int(tPos[1]) 
         scop_fam += hit["scop"] + " "
    #    #print hit["scop"]
         query += hit["query"] + " "
         evals += hit["e-value"] +" "
         querypos += hit["querypos"] + " "
         targetpos += hit["templatepos"] + " "
    print ">" + hitId + "_1 " + scop_fam +"|"+evals+ "|" + query + "|" + querypos + "|" +targetpos
    print dbSeq 
