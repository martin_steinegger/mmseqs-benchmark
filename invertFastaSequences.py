from itertools import groupby
import random
from optparse import OptionParser
from math import floor
def reverse_string(a_string):
    return a_string[::-1]

def fasta_iter(fasta_name):
    """
    given a fasta file. yield tuples of header, sequence
    """
    fh = open(fasta_name)
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = header.next()[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in faiter.next())
        yield header, seq



parser = OptionParser()

parser.add_option("-i", "--in", dest="fasta",
                  help="Path to fasta file.", metavar="STRING")
(options, args) = parser.parse_args()



for header, seq in fasta_iter(options.fasta):
    #0                        1            2                3           4
    print ">"+header 
    print reverse_string(seq) 


